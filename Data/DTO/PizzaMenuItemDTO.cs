﻿namespace FPSC.Data.DTO
{
    public class PizzaMenuItemDTO
    {
        public int PizzaMenuItemId { get; set; }
        public decimal Price { get; set; }
        public string PizzaTypeName { get; set; }
        public string PizzaSizeName { get; set; }
    }
}
