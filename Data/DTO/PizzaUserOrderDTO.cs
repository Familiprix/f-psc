﻿using System.Collections.Generic;

namespace FPSC.Data.DTO
{
    public class PizzaUserOrderDTO
    {
        public string UserName { get; set; }
        public List<PizzaUserOrderItemDTO> OrderItems { get; set; }
    }

    public class PizzaUserOrderItemDTO
    {
        public int PizzaTypeId { get; set; }
        public int NumberOfSlices { get; set; }
    }
}
