﻿using System.Collections.Generic;
using System.Linq;

namespace FPSC.Data.DTO
{
    public abstract class Result : IResult
    { 
        public static Result<T> From<T>(IResult<T> result)
        {
            return new Result<T>(result.Data).WithMessages(result.Messages);
        }

        public static Result<T> From<T>(T data)
        {
            return new Result<T>(data);
        }

        protected Result(IEnumerable<string> messages = null)
        {
            if (messages != null)
                messages.ToList().ForEach(Messages.Add);
        }

        public virtual bool Success => Messages.Count == 0;

        public List<string> Messages { get; } = new List<string>();
    }

    public partial class Result<T> : Result, IResult<T>
    {
        public T Data { get; set; }

        public override bool Success => base.Success && EqualityComparer<T>.Default.Equals(Data, default(T)) == false;

        private Result(IEnumerable<string> messages = null) : base(messages) { }

        public static Result<T> NullResult() => new Result<T>();

        public Result(T data, IEnumerable<string> messages = null) : base(messages) => Data = data;

        public void Deconstruct(out T data, out List<string> messages)
        {
            data = Data;
            messages = Messages;
        }
    }
}
