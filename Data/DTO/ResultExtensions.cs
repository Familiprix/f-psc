﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FPSC.Data.DTO
{
    public static class ResultExtension
    {
        public static Result<T> WithMessages<T>(this Result<T> self, IEnumerable<string> messages)
        {
            messages.ToList().ForEach(self.Messages.Add);
            return self;
        }

        public static Result<T> WithMessage<T>(this Result<T> self, string message)
        {
            self.Messages.Add(message);
            return self;
        }

        public static Result<A> Unwrap<A>(this Result<Result<A>> self)
        {
            if (self.Success)
                return Result.From(self.Data.Data).WithMessages(self.Data.Messages).WithMessages(self.Messages);
            else
                return GetEmptyResultWithMessages<A>(self).WithMessages(self.Data?.Messages ?? new List<string>());
        }

        public static Result<B> Map<A, B>(this Result<A> self, Func<A, B> func) => CreateMapFunction(self, func);

        public static Result<B> Bind<A, B>(this Result<A> self, Func<A, Result<B>> func) => CreateMapFunction(self, func).Unwrap();

        public static Result<T> Do<T>(this Result<T> self, Action<T> action)
        {
            if (self.Success)
                action(self.Data);

            return self;
        }

        public static Result<T> Validate<T>(this Result<T> self, Func<T, List<string>> validationFunc) =>
            self.Bind(data => Result.From(data).WithMessages(validationFunc(data)));

        private static Result<B> CreateMapFunction<A, B>(Result<A> input, Func<A, B> func)
        {
            if (input.Success)
                return Result.From(func(input.Data)).WithMessages(input.Messages);
            else
                return GetEmptyResultWithMessages<B>(input);
        }

        private static Result<B> GetEmptyResultWithMessages<B>(Result self) =>
            Result<B>.NullResult().WithMessages(self.Messages);
    }
}
