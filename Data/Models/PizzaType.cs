﻿namespace FPSC.Data.Models
{
    public class PizzaType
    {
        public int PizzaTypeId { get; set; }
        public string Name { get; set; }
    }
}
