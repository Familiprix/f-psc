﻿using AutoMapper;
using FPSC.Data;
using FPSC.Data.DTO;
using FPSC.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FPSC.PizzaService
{
    public class PizzaRioService : IPizzaService
    {
        private readonly PizzaContext _context;
        private readonly IMapper _mapper;

        public PizzaRioService(PizzaContext context, IMapper mapper)
        {
            this._context = context;
            this._mapper = mapper;
        }

        public List<PizzaMenuItemDTO> GetMenu()
        {
            // The use of "_mapper" let you esily convert an object to an other using properties reflection
            // You can define your own mapping method inside the "PizzaProfile" constructor
            var menu = _context.PizzaMenuItem.Include(item => item.PizzaSize)
                                             .Include(item => item.PizzaType)
                                             .Select(_mapper.Map<PizzaMenuItemDTO>);
            
            return menu.ToList();
        }

        public IResult<PizzaOrderDTO> GetPizzaOrder(int orderId)
        {
            throw new NotImplementedException();
        }

        public IResult<PizzaOrderDTO> CreatePizzaOrder()
        {
            var order = new PizzaOrder(){
                Date = DateTime.Now,
                PizzaOrderItems = new List<PizzaOrderItem>()
            };

            _context.PizzaOrder.Add(order);

            try
            {
                _context.SaveChanges();
            }
            catch(Exception e)
            {
                var messages = new []{e.Message};

                return Result<PizzaOrderDTO>.NullResult()
                    .WithMessages(messages);
            }

            return Result.From(order)
                .Map(_mapper.Map<PizzaOrderDTO>);
        }

        public IResult AddPizzaOrderItem(int orderId, PizzaUserOrderDTO userOrder)
        {
            throw new NotImplementedException();
        }
    }
}
